# -*- coding: utf-8 -*-
"""
   @file                task_controller.py
   @brief               Task controller for the closed Loop driver 
   @details             This file is responsible for logic of the closed loop driver. 
                        The main purpose of this file is the pass and receive data to the closed loop controller driver. 
                        This task controller also manages the frequency at which the closed loop driver is called. 
   @author              Eddy Rodriguez
   @author              Chloe Chou
   @copyright           License Info
   @date                November 2021
Created on Sat Oct 30 16:27:42 2021

"""


import ClosedLoop, utime

class Task_Controller:
    '''
    @brief Task Controller Class where logic takes place 
    '''
    def __init__(self,period, delta, omega_ref, kp, enc_duty):
        '''
        @brief Object contructor for Task_Controller class.
        @param period 
               delta This parameter is the measured motor velocity in rad/sec
               omega_ref This parameter is the desired velocity in rad/sec inputed by the user
               enc_duty This parameter is the encoder duty value

        '''
        self.delta = delta
        self.omega_ref = omega_ref
        self.kp = kp
        self.closedloop = ClosedLoop.ClosedLoop(self.delta,self.omega_ref,self.kp)
        self.point_value = self.closedloop.run()
        self.enc_duty = enc_duty
                
        self.period = period
        self.next_time = utime.ticks_add(utime.ticks_us(), period)
        self.next_time_collect = utime.ticks_add(utime.ticks_us(), period)

    def run(self):
        '''
        @brief This method calls the closed Loop driver and controls the rate at which the driver class is ran. 
        @return closedloop.run() Returns the new duty that located in the driver run method
        @return point_value Returns the previous calculated duty if not enough time has passed to calculate a new duty.

        '''
        self.current_time = utime.ticks_us()
        if (utime.ticks_diff(self.current_time, self.next_time) >= 0):
            self.point_value = self.closedloop.run()
            return self.closedloop.run()
        else:
            return self.point_value
        
        self.next_time = utime.ticks_add(self.next_time, self.period)