'''
    @file       task_user.py
    @brief      This file is responsible for the user interface of the motor commands.
    @details    Depending on the input of the user the task_user.py will zero the
                motor encoder, print the motor encoder position, print the motor
                encoder delta, collect data for 30 seconds and print as a list,
                and enter duty cycles. This can be done with either Motor 1 or 
                Motor 2 as specified by capital/lowercase letters.
                
    @image html SStask_user.jpg "State Space Diagram:Task Diagram" width=600px
    @image html lab2task.png "Task Diagram" width=600px

    @author: Eddy Rodriguez
    @author: Chloe Chou
    @date: November 1, 2021
'''
import utime,pyb

S0_INIT = 0
S1_WAIT_FOR_CHAR = 1
S2_ENTER_KP = 2
S3_ENTER_DATA = 3
S4_STEP_MOTOR = 4
S5_PRINT_DATA = 5
S6_FAULT = 6

class Task_User:
    '''
    @brief
    @details
    '''
    def __init__ (self,period, enc_pos_1,enc_pos_2, delta_pos_1,delta_pos_2, 
                  enc_duty_1, enc_duty_2, enable, fault_found, 
                  kp_1, omega_ref_1, kp_2, omega_ref_2):

        '''
        @brief Initializes and returns a task_user object.
        @param period controls the frequency at which the encoder returns data 
               enc_pos_1    Contains the encoder position of Motor 1
               enc_pos_2    Contains the encoder position of Motor 2
               delta_pos_1  Contains the delta of Motor 1 encoder over a period 
               delta_pos_2  Contains the delta of Motor 2 encoder over a period  
               enc_duty_1   Contains the duty cycle of Motor 1
               enc_duty_2   Contains the duty cycle of Motor 2
               enable       Contains information to re-enable motors
               fault_found  Contains information if a fault was detected.
        '''
        self.enc_pos_1 = enc_pos_1
        self.delta_pos_1 = delta_pos_1
        self.enc_duty_1 = enc_duty_1
        self.kp_1 = kp_1
        self.omega_ref_1 = omega_ref_1
        
        
        self.enc_pos_2 = enc_pos_2
        self.delta_pos_2 = delta_pos_2
        self.enc_duty_2 = enc_duty_2
        self.kp_2 = kp_2
        self.omega_ref_2 = omega_ref_2
        
        
        self.period = period
        self.next_time = utime.ticks_add(utime.ticks_us(), period)
        self.next_time_collect = utime.ticks_add(utime.ticks_us(), period)
        self.state = S0_INIT
        self.interrupt = False
        
        self.time = [utime.ticks_us()]
        self.timediff = []
        
        self.my_list_runs = []
        self.my_list_seconds = []
        self.my_list_velocity = []
        self.my_list_duty = []
        
        self.runs = 1
        self.motor_number = 0
        self.num_st = ""
        self.kp_str = ""

        self.ser_port = pyb.USB_VCP()
        
        self.enable = enable
        self.enable.write(False)
        
        self.fault_found = fault_found
        
    def run(self):
        '''
        @brief      This function runs different states in the task_user command.
        @details    This function waits for input of the user to call different
                    states of the motor as printed on the screen for the user. 
                    It will leave the user input command phase if collecting 
                    data, printing data, taking data input for duty cycles, or 
                    if a fault is triggered.
        '''
        self.current_time = utime.ticks_us()
        if (utime.ticks_diff(self.current_time, self.next_time) >= 0):
            if self.state == S0_INIT:
                print("______________________________________________________")
                print("|Welcome, here are a list of commands for this device.|")
                print("|1 - Enter a duty cycle for Motor 1                   |")
                print("|2 - Enter a duty cycle for Motor 2                   |")
                print("|_____________________________________________________|")
                self.state = S1_WAIT_FOR_CHAR
                
            elif self.state == S1_WAIT_FOR_CHAR:
                if(self.fault_found.read()):
                        self.state = S6_FAULT
                
                if(self.ser_port.any()):
                    char_decoded = self.ser_port.read(1).decode()
                    
                    if(char_decoded == '1'):
                        print ("Please enter Kp for Motor 1")
                        self.motor_number = 1
                        self.state = S2_ENTER_KP
                    
                    elif(char_decoded == '2'):
                        print ("Please enter Kp for Motor 2")
                        self.motor_number = 2
                        self.state = S2_ENTER_KP
                        
                        
            elif self.state == S2_ENTER_KP:
                if(self.fault_found.read()):
                        self.state = S6_FAULT
                
                if(self.ser_port.any()):
                        char_in = self.ser_port.read(1).decode()
                        
                        if(char_in.isdigit()): # Adds to string only if the number is a digit
                            self.kp_str = self.kp_str + char_in
                            self.ser_port.write(char_in)
                        
                        elif (char_in == '-'): # Only adds to string if the minus sign is the first character entered
                            if (len(self.kp_str)==0):
                                self.kp_str = self.kp_str + char_in
                                self.ser_port.write(char_in)
                        
                        elif (char_in == '\x7F'): # Backspaces last key and changes num_st appropriately
                            if (len(self.kp_str)!=0):
                                self.kp_str = self.kp_str[:-1]
                                self.ser_port.write(char_in)
                        
                        elif (char_in == '.'): # If the string does not already have a decimal, add one
                            if self.kp_str.find(char_in) == -1:
                                self.kp_str = self.kp_str + char_in
                                self.ser_port.write(char_in)
                        elif (char_in == '\r'or char_in == '\n'): # Submit entered duty
                            if (len(self.kp_str)==0):
                                self.kp_str = "0.0"
                            print ("")
                            kp_num = float (self.kp_str)
                            
                            if self.motor_number == 1:
                                print('Motor 1 given k of {:}.'.format(kp_num))
                                self.kp_1.write(kp_num)
                                
                                print('Please enter velocity for Motor 1.')
                                self.state = S3_ENTER_DATA
                                self.kp_str = "" 
                                kp_num = 0
                                
                            elif self.motor_number == 2:
                                print('Motor 2 given k of {:}.'.format(kp_num))
                                self.kp_2.write(kp_num)
                                print('Please enter velocity for Motor 2.')
                                self.state = S3_ENTER_DATA
                                self.kp_str = ""
                                kp_num = 0
        
            elif self.state == S3_ENTER_DATA:
                if(self.fault_found.read()):
                        self.state = S6_FAULT
                
                if(self.ser_port.any()):
                        char_in = self.ser_port.read(1).decode()
                        
                        if(char_in.isdigit()): # Adds to string only if the number is a digit
                            self.num_st = self.num_st + char_in
                            self.ser_port.write(char_in)
                        
                        elif (char_in == '-'): # Only adds to string if the minus sign is the first character entered
                            if (len(self.num_st)==0):
                                self.num_st = self.num_st + char_in
                                self.ser_port.write(char_in)
                        
                        elif (char_in == '\x7F'): # Backspaces last key and changes num_st appropriately
                            if (len(self.num_st)!=0):
                                self.num_st = self.num_st[:-1]
                                self.ser_port.write(char_in)
                        
                        elif (char_in == '.'): # If the string does not already have a decimal, add one
                            if self.num_st.find(char_in) == -1:
                                self.num_st = self.num_st + char_in
                                self.ser_port.write(char_in)
                        elif (char_in == '\r'or char_in == '\n'): # Submit entered duty
                            if (len(self.num_st)==0):
                                self.num_st = "0.0"
                            print ("")
                            num_doub = float (self.num_st)
                            
                            if self.motor_number == 1:
                                print('Motor 1 will run at {:} radians/second'.format(num_doub))
                                print('Running closed loop. Press s to interrupt early.')
#                                self.enc_duty_1.write(self.num_st)
                                self.omega_ref_1.write(num_doub)
                                self.state = S4_STEP_MOTOR
                                num_doub = 0
                                self.num_st = ""
                                
                            elif self.motor_number == 2:
                                print('Motor 2 will run at {:} radians/second'.format(num_doub))
                                print('Running closed loop. Press s to interrupt early.')
#                                self.enc_duty_2.write(self.num_st)
                                self.omega_ref_2.write(num_doub)
                                self.state = S4_STEP_MOTOR
                                num_doub = 0
                                self.num_st = "" 
                
            elif self.state == S4_STEP_MOTOR:
                self.current_time_collect = utime.ticks_us()
                if (utime.ticks_diff(self.current_time_collect, self.next_time_collect) >= 0):
                    if self.motor_number == 1:
                        self.my_list_velocity.append(self.delta_pos_1.read())
                        self.my_list_runs.append(self.runs)
                        self.my_list_duty.append(self.enc_duty_1.read())
                        self.runs = self.runs + 1
                        
                    if self.motor_number == 2:
                        self.my_list_velocity.append(self.delta_pos_2.read())
                        self.my_list_duty.append(self.enc_duty_2.read())
                        self.my_list_runs.append(self.runs)
                        self.runs = self.runs + 1
                        
                    if(self.ser_port.any()):
                        char_in = self.ser_port.read(1).decode()
                        if(char_in == 's' or char_in == 'S'): 
                            self.interrupt = True
                            print('Collection interrupted. Ending data collection.')
                            self.state = S5_PRINT_DATA
                            
                    if(self.runs >= 100):
                        self.interrupt = True
                        print('End data collection.')
                        self.state = S5_PRINT_DATA
                    self.next_time_collect = utime.ticks_add(self.next_time_collect, self.period)   

            elif self.state == S5_PRINT_DATA:
                print('Printing data.')
                print('Motor {:} over {:} points:'.format(self.motor_number, len(self.my_list_runs)))
                
                for x in range(len(self.my_list_runs)):
                    self.my_list_seconds.append(round(self.my_list_runs[x]*(self.period/1000000), 1))
                    #print('At second {:}, velocity is {:} rad/sec with a duty cycle of {:}'.format
                          #(self.my_list_seconds[x], self.my_list_velocity[x], self.my_list_duty[x]))
                    print(self.my_list_seconds[x], self.my_list_velocity[x], self.my_list_duty[x])
                          

                self.my_list_velocity.clear()
                self.my_list_runs.clear()
                self.my_list_duty.clear()
                self.my_list_seconds.clear()
                self.runs = 1
                self.omega_ref_1.write(0)
                self.omega_ref_2.write(0)
                self.state = S1_WAIT_FOR_CHAR
            
            elif self.state == S6_FAULT:
                if(self.ser_port.any()):
                    char_decoded = self.ser_port.read(1).decode()
                    if(char_decoded=='c'or char_decoded=='C'):
                        self.enable.write(True)
                        print("Fault is cleared.")
                        self.state = S1_WAIT_FOR_CHAR
            else:
                raise ValueError('Invalid State') 
            self.next_time = utime.ticks_add(self.next_time, self.period)
            