'''
    @file       lab3main.py
    @brief      Main file is designed to give tasks.
    @details    This file does not deal with the hardware directly but 
                assigns task to the task_user.py and task_encoder.py files. 
                Task to individual encoders can be assigned here and parameters
                like period, the rate at which the encoder responds, can
                be controlled here as well.
    @author: Eddy Rodriguez
    @author: Chloe Chou
    @date: November 1, 2021
'''

import task_encoder, task_user, DRV8847, task_motor, task_controller, shares, pyb


def main():
    ''' 
    @brief      The main program
    @details    Tasks for the individual motors, encoders, and the user interface
                is established here, as well as the data that these tasks 
                collectively share.
    ''' 
    
    #Shares for Motor 1: position, delta, zero, and duty
    enc_pos_1 =  shares.Share(0)
    delta_pos_1 =shares.Share(0)
    enc_duty_1=  shares.Share(0)
    kp_1 =  shares.Share(0)
    omega_ref_1 =  shares.Share(0)
    
    
    #Shares for Motor 1: position, delta, zero, and duty
    enc_pos_2 = shares.Share(0)
    delta_pos_2 = shares.Share(0)
    enc_duty_2 = shares.Share(0)
    kp_2 =  shares.Share(0)
    omega_ref_2 =  shares.Share(0)
    
    ## @brief   A variable enable that is true whenever there is not a fault.
    ## @details This variable is written in task_user.py and is set to True
    ##          if the 'c' key is pressed to reset the fault condition.
    enable = shares.Share()
    
    ## @brief   A variable fault_found that triggers during a fault.
    ## @details This variable is written in DRV8847.py and is set to True
    ##          if a fault is detected.
    fault_found = shares.Share(False)

    motor_drv = DRV8847.DRV8847(pyb.Pin.cpu.A15, pyb.Pin.cpu.B2, fault_found)
    motor_drv.enable()  # Enable the motor driver
    
    motor_1 = motor_drv.motor(1,2, pyb.Pin.cpu.B4, pyb.Pin.cpu.B5,3) #Motor Object 1
    motor_2 = motor_drv.motor(3,4, pyb.Pin.cpu.B0, pyb.Pin.cpu.B1,3) #Motor object 2
    
    task_controller1 = task_controller.Task_Controller(100000, delta_pos_1,omega_ref_1,kp_1, enc_duty_1)#object for task controller motor 1
    task_controller2 = task_controller.Task_Controller(100000, delta_pos_2,omega_ref_2,kp_2, enc_duty_2)#object for task controller motor 1

    task1 = task_user.Task_User(100000,enc_pos_1,enc_pos_2, delta_pos_1,delta_pos_2, 
                                enc_duty_1, enc_duty_2,enable, 
                                fault_found,kp_1,omega_ref_1,kp_2, omega_ref_2)
    
    
    task2 = task_encoder.Task_Encoder(100000,4,enc_pos_1, delta_pos_1)
    task3 = task_encoder.Task_Encoder(100000,8,enc_pos_2, delta_pos_2)
    task4 = task_motor.Task_Motor(motor_1,motor_drv,enable)
    task5 = task_motor.Task_Motor(motor_2,motor_drv,enable)
    
    #first trying feedback for motor 1

    
    
    
    while(True):

        task1.run()
        task2.run()
        task3.run()
        new_duty_1 = task_controller1.run()
        new_duty_2 = task_controller2.run()
        
        task4.run(float(new_duty_1))
        enc_duty_1.write(-new_duty_1)
        task5.run(float(new_duty_2))
        enc_duty_2.write(new_duty_2)
#        if (new_duty_1 != 0):
#            print('Duty 1:', new_duty_1)
#        if (new_duty_2 != 0):
#            print('Duty 2:', new_duty_2)
#        if (omega_ref_1.read() !=0 or omega_ref_2.read() !=0):
#            if (omega_ref_1.read() == delta_pos_1.read() or omega_ref_2.read() == delta_pos_2.read()):
#                print('Measured delta is equal to input delta')
                
if __name__ == '__main__':
    main()