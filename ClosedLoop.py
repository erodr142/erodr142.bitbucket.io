# -*- coding: utf-8 -*-
'''@file                ClosedLoop.py
   @brief               ClosedLoop feedback control for the DC motors
   @details             A PID controller is used to reach desired motor velocity. The user inputs a Kp, proportional gain, 
                        and a desired motor velocity which are both shared to this closedloop driver. This driver calculates a 
                        new duty cycle that is then shared back to the task_motor. 
   @author              Eddy Rodriguez
   @author              Chloe Chou
   @copyright           License Info
   @date                November 2021
'''
"""
Created on Sat Oct 30 16:18:45 2021

"""
class ClosedLoop:
    '''
    @brief Close-loop controller class
    @details Close loop controller class to controll the speed of the motor 
    '''
    def __init__(self,delta,omega_ref,kp):
        '''
        @brief Object contructor for Closed Loop class
        @param delta This parameter is the measured motor velocity in rad/sec 
               omega_ref This parameter is the desired velocity inputed by the user
               kp This parameter is the proportional gain for the closeloop controller

        '''
        self.omega_ref = omega_ref
        self.delta = delta
        self.kp = kp
        self.error = [0]
        self.error_sum = 0
        self.past_error = self.omega_ref.read() - self.delta.read()
        
        
    def run(self):
        '''
        @brief Runs controller function 
        @details The function runs the closedloop feedback system. New calculated duty is below -100 or above 100
                 then the duty is set to the maximum duty of either -100 or 100. 
        @return duty This function returns the new calculated duty cycle for the DC motor 
        '''
        # Figuring out the error for integral.
        # The 1/100 is for the time difference
        error = self.omega_ref.read() - self.delta.read()
        self.error_sum += error*(1/100)
        
        #Figuring out the error for derivative.
        # The 1/100 is for the time difference.
        error_difference = error - self.past_error
        self.past_error = error
        
        
        PID = self.kp.read()*(error) + 1*self.error_sum + 1*error_difference
        
        self.duty = PID/-3.3
        if self.duty >100:
            self.duty = 100
        if self.duty <-100:
            self.duty = -100
        return self.duty
        
    def get_Kp(self,kp):
        '''
        @brief Method to retrieve the proportional controller gain value
        @param Stores the proportional gain kp inputed by the user 
        @returns The proportional gain kp
        '''
        return self.kp

     
    def set_Kp(self, kp):
        '''
        @brief Sets the controller gain value and allows modification for the controller gain value
        @param kp The proportional controller gain value

        '''
        self.kp = kp

