var searchData=
[
  ['scan_0',['scan',['../classtouchscreen1_1_1Touchscreen.html#ae9687e79641446acd19c6d620284d3a3',1,'touchscreen1::Touchscreen']]],
  ['scan_5fraw_1',['scan_raw',['../classtouchscreen1_1_1Touchscreen.html#aff5f4016f0e29a8f0405abc6c8bb65fd',1,'touchscreen1::Touchscreen']]],
  ['set_5fduty_2',['set_duty',['../classDRV8847_1_1Motor.html#ae2e6c0feeb46de3f93c35e7f25a79a8b',1,'DRV8847.Motor.set_duty()'],['../classDRV88471_1_1Motor.html#af1c879b132b3ba4482251533a0d36d84',1,'DRV88471.Motor.set_duty()']]],
  ['set_5fduty_5fcycle_3',['set_duty_cycle',['../classmotor_1_1Motor.html#aab6d345560661d43ccdb60a94165cc35',1,'motor::Motor']]],
  ['set_5fkp_4',['set_Kp',['../classClosedLoop_1_1ClosedLoop.html#aa6b1cd603a27ba90b9e3b4dda66d9591',1,'ClosedLoop::ClosedLoop']]],
  ['set_5fposition_5',['set_position',['../classencoder_1_1Encoder.html#a097746ac59abf28e6567f5604fe83c1f',1,'encoder::Encoder']]]
];
