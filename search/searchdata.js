var indexSectionsWithContent =
{
  0: "_abcdefghlmnpqrstuw",
  1: "bcdemqst",
  2: "h",
  3: "bcdehlmst",
  4: "_acdefglmnprsuw",
  5: "cdprst",
  6: "efl"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Pages"
};

