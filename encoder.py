'''
    @file    encoder.py
    @brief   Task for collecting data from the hardware. 
    @details This file interacts directly with the hardware of the Nucleo to
             read motor encoders and return information as called.
            
    @author: Eddy Rodriguez
    @author: Chloe Chou
    @date: November 1, 2021
'''
import pyb,math


class Encoder:
    ''' @brief  Interface with quadrature encoders
        @details Creates a class for the encoder to call encoder position, 
        change in encoder position, and datums the motor. 
        
    '''

    def __init__(self,timNum):
        ''' @brief Constructs an encoder object
            @param timNum   Corresponds to the timer channel that the
                            encoder is connected to. 
        '''
        self.datumPosition = 0
        self.old_counter = 0
        self.delta = 0
        self.count = 0
        self.position = 0
        self.cap = 65535
        self.oldDelta = -1
        
        if (timNum ==4):
            self.tim = pyb.Timer(timNum,prescaler = 0, period = 65535)
            self.TIM4_CH1 = self.tim.channel(1, mode = pyb.Timer.ENC_A, pin = pyb.Pin.cpu.B6)
            self.TIM4_CH2 = self.tim.channel(2, mode = pyb.Timer.ENC_B, pin = pyb.Pin.cpu.B7)
        elif(timNum==8):
            self.tim = pyb.Timer(timNum,prescaler = 0, period = 65535)
            self.TIM4_CH1 = self.tim.channel(1, mode = pyb.Timer.ENC_A, pin = pyb.Pin.cpu.C6)
            self.TIM4_CH2 = self.tim.channel(2, mode = pyb.Timer.ENC_B, pin = pyb.Pin.cpu.C7)
    
    def get_count(self):
        '''
            @brief   Gets encoder timer  
            @return  The value of the timer encoder
        '''
        return self.tim.counter()
    
    def update(self):
        ''' @brief      Updates encoder position and delta
            @details    Updates the encoder postion,delta, and 
                        handles the enconter over and underflow. 
        '''
        
        self.count = self.get_count()
        self.delta = self.count - self.old_counter
        self.old_counter = self.count
        if (abs(self.delta) >= self.cap/2):
                if(self.delta >= self.cap/2):
                    self.delta-=self.cap
                else:
                    self.delta+=self.cap
        if(self.delta!=self.oldDelta):
            self.datumPosition+= self.delta
            self.oldDelta = self.delta

    def get_position(self): 
        ''' @brief Returns encoder position
            @details
            @return The position of the encoder shaft
        '''
        
        return self.datumPosition*(2*math.pi/4000)
        
    def set_position(self, position):
        ''' @brief Sets encoder position
            @details
            @param position The new position of the encoder shaft
        '''
        
        self.datumPosition = self.position*(2*math.pi/4000)
    
    def get_delta(self):
        ''' @brief Returns encoder delta
            @details
            @return The change in position of the encoder shaft
            between the two most recent updates
        '''
        return self.delta*(2*math.pi/4000)*10
        
