/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Eddy Rodriguez's Mechatronic Project", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Lab Short Descriptions", "index.html#sec_lab", null ],
    [ "Lab0", "Lab0.html", [
      [ "Lab 0x00 Fibonacci", "Lab0.html#sec_lab0", null ]
    ] ],
    [ "Lab1", "Lab1.html", [
      [ "Lab 1 Flashing LED", "Lab1.html#sec_lab1", null ]
    ] ],
    [ "Lab2", "Lab2.html", [
      [ "Lab 2 Encoder", "Lab2.html#sec_lab2", null ]
    ] ],
    [ "Lab3", "Lab3.html", [
      [ "Lab 3 Motor", "Lab3.html#sec_lab3", null ]
    ] ],
    [ "Lab4", "Lab4.html", [
      [ "Lab 4 Closedloop Controller", "Lab4.html#sec_lab4", null ]
    ] ],
    [ "Lab5", "Lab5.html", [
      [ "Lab 5 Inertial Measurement Unit", "Lab5.html#sec_lab5", null ]
    ] ],
    [ "Final_Term_Project", "Final_Term_Project.html", [
      [ "Final Term Project", "Final_Term_Project.html#sec_lab6", null ]
    ] ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ],
      [ "Package Functions", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"BN0055_8py.html",
""
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';